#!/usr/bin/env python3
import argparse
from pathlib import Path
from base64 import b64encode
import logging

from toolz import pipe, compose

log = logging.getLogger('midi_to_b64')
log.addHandler(logging.NullHandler())

def midi_to_b64(path: Path):
    return pipe(
        path.read_bytes(),
        b64encode
    )

def bytes_to_data(data: bytes):
    return f'data:audio/midi;base64,{data.decode()}'

midi_to_data = compose(bytes_to_data, midi_to_b64)

def transform_midi_path(path: Path, outdir: Path):
    new = Path(outdir, f'{path.stem}.b64')
    if not new.exists() or new.stat().st_ctime < path.stat().st_ctime:
        log.info(f'Writing {path} to {new}')
        new.write_text(midi_to_data(path))
    else:
        log.info(f'base64 data {new} already exists.')

def main():
    logging.basicConfig(level=logging.INFO)
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'paths', nargs='+', type=lambda p:Path(p).resolve(),
        help=('MIDI paths to be converted into base64'),
    )
    parser.add_argument(
        '-d', '--dir', type=lambda p:Path(p).resolve(),
        default=Path('./b64').resolve(),
        help=('Path to store base64-encoded versions of'
              ' input MIDI files'),
    )

    args = parser.parse_args()

    if not args.dir.exists():
        args.dir.mkdir(parents=True, exist_ok=True)

    for p in args.paths:
        transform_midi_path(p, args.dir)

if __name__=='__main__':
    main()
