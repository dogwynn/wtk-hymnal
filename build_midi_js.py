#!/usr/bin/env python3
import argparse
from pathlib import Path
from base64 import b64encode
import logging
import json

from toolz import pipe, compose, curry
from toolz.curried import map

log = logging.getLogger('midi_to_b64')
log.addHandler(logging.NullHandler())

@curry
def vmap(func, iterable):
    return map(lambda v: func(*v), iterable)

def get_js_source(path: Path):
    return pipe(
        [
            "inc/shim/Base64.js",
            "inc/shim/Base64binary.js",
            "inc/shim/WebAudioAPI.js",
            "inc/shim/WebMIDIAPI.js",
            "inc/jasmid/stream.js",
            "inc/jasmid/midifile.js",
            "inc/jasmid/replayer.js",
            "build/MIDI.js",
            'examples/soundfont/acoustic_grand_piano-ogg.js',
            # "js/midi/audioDetect.js",
            # "js/midi/gm.js",
            # "js/midi/loader.js",
            # "js/midi/player.js",
            # "js/midi/plugin.audiotag.js",
            # "js/midi/plugin.webaudio.js",
            # "js/midi/plugin.webmidi.js",
            "js/util/dom_request_xhr.js",
            "js/util/dom_request_script.js",
        ],
        map(lambda p: Path(path, p).resolve()),
        map(lambda p: f'// {p.name}\n\n{p.read_text()}'),
        lambda sources: '\n\n'.join(sources)
    )

def get_hymn_data(path: Path):
    return pipe(
        path.glob('*.b64'),
        sorted,
        enumerate,
        vmap(lambda i, p: {'id': i, 'name': p.stem, 'data': p.read_text()}),
    )

JS='''

function play_hymn(name) {{
  MIDI.Player.loadFile(hymns[name]['data'], MIDI.Player.start);
}}
'''

SOURCE = '''
{midi_source}

hymns = {hymn_data}

{control_source}
'''
        
def main():
    logging.basicConfig(level=logging.INFO)
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-m' ,'--midijs', type=lambda p:Path(p).resolve(), required=True,
        help=('Path to MIDI.js source directory'),
    )
    parser.add_argument(
        '-b' ,'--b64', type=lambda p:Path(p).resolve(), required=True,
        help=('Path to directory where base64-encoded hymn data is contained'),
    )
    parser.add_argument(
        '-o', '--outpath', type=Path,
    )

    args = parser.parse_args()

    midi_source = get_js_source(args.midijs)
    hymn_data = {h['name']: h for h in get_hymn_data(args.b64)}

    source = SOURCE.format(
        midi_source=midi_source,
        hymn_data=json.dumps(hymn_data, indent=2),
        control_source=JS,
    )
    
    if args.outpath:
        args.outpath.write_text(source)
    else:
        print(source)
    
    

if __name__=='__main__':
    main()
