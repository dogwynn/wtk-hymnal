# Adding MIDI to wtkhymnal.org

Added the following 3rd-party modules:

- [Advanced Custom Fields](https://www.godaddy.com/garage/3-ways-to-insert-javascript-into-wordpress-pages-or-posts/)

Changed the following templates:

- header.php
- footer.php
- content-page.php
